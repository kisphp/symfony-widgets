<?php

namespace WidgetsBundle\Services;

use Doctrine\ORM\EntityManager;
use WidgetsBundle\Entity\Repository\WidgetsZoneRepository;

class WidgetsMenuReader
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * MenuReader constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \WidgetsBundle\Entity\WidgetsZoneEntity[]
     */
    public function getLayoutZonesForAdminMenu()
    {
        /** @var WidgetsZoneRepository $repository */
        $repository = $this->em->getRepository('WidgetsBundle:WidgetsZoneEntity');

        return $repository->getLayoutZonesForAdminMenu();
    }
}

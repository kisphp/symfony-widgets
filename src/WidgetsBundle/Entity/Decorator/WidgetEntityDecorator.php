<?php

namespace WidgetsBundle\Entity\Decorator;

use WidgetsBundle\Entity\WidgetsEntity;

class WidgetEntityDecorator
{
    const TITLE = 'title';
    const SPECIAL_WORD = 'special_word';

    /**
     * @var WidgetsEntity
     */
    protected $widget_entity;

    /**
     * @param WidgetsEntity $widget_entity
     */
    public function __construct(WidgetsEntity $widget_entity)
    {
        $this->widget_entity = $widget_entity;
    }

    /**
     * @return string
     */
    public function getSlideshowTitle()
    {
        $boxData = $this->widget_entity->getBoxData();
        $boxData[self::TITLE] = strtolower($boxData[self::TITLE]);

        if (array_key_exists(self::SPECIAL_WORD, $boxData)) {
            $boxData[self::SPECIAL_WORD] = strtolower($boxData[self::SPECIAL_WORD]);

            return str_replace($boxData[self::SPECIAL_WORD], '<span>' . $boxData[self::SPECIAL_WORD] . '</span>', $boxData[self::TITLE]);
        }

        return $boxData[self::TITLE];
    }
}

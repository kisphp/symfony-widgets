<?php

namespace WidgetsBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use WidgetsBundle\Entity\WidgetsEntity;

class WidgetsAttachedRepository extends EntityRepository
{
    /**
     * @param WidgetsEntity $entity
     *
     * @return array
     */
    public function findAttachedByWidget(WidgetsEntity $entity)
    {
        return $this->findBy([
            'id_widget' => $entity->getId(),
        ]);
    }
}

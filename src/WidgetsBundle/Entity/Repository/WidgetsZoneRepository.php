<?php

namespace WidgetsBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\Utils\Status;

class WidgetsZoneRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getUndeletedWidgetZones()
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.status != :status')
            ->setParameter('status', Status::DELETED)
            ->getQuery()
        ;

        return $query->getResult();
    }

    /**
     * @return array
     */
    public function getLayoutZonesForAdminMenu()
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.status = :status')
            ->setParameter('status', Status::ACTIVE)
            ->getQuery()
        ;

        return $query->getResult();
    }
}

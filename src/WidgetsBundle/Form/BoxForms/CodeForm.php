<?php

namespace WidgetsBundle\Form\BoxForms;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use WidgetsBundle\Form\AbstractWidgetForm;

class CodeForm extends AbstractWidgetForm
{
    const FIELD_CODE = 'code';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(self::FIELD_CODE, TextareaType::class, [
            'attr' => [
                'rows' => '10',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return 'WidgetsBundle:Templates:code.html.twig';
    }
}

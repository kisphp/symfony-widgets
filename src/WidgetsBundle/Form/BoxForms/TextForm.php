<?php

namespace WidgetsBundle\Form\BoxForms;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use WidgetsBundle\Form\AbstractWidgetForm;

class TextForm extends AbstractWidgetForm
{
    const FIELD_TEXT = 'text';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(self::FIELD_TEXT, TextareaType::class, [
            'attr' => [
                'rows' => 10,
                'class' => 'tinymce',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return 'WidgetsBundle:Templates:text.html.twig';
    }
}

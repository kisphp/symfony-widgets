<?php

namespace WidgetsBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use WidgetsBundle\Entity\WidgetsEntity;

abstract class AbstractWidgetForm extends AbstractType
{
    /**
     * @var WidgetsEntity
     */
    protected $entity;

    /**
     * @return string
     */
    abstract public function getTemplate();

    /**
     * @param WidgetsEntity $entity
     */
    public function setEntity(WidgetsEntity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param ContainerInterface $container
     * @param array|object $postData
     */
    public function postForm(ContainerInterface $container, $postData)
    {
        // this method will be automatically called after success submit of the form for custom processing
    }

    /**
     * @param ContainerInterface $container
     * @param WidgetsEntity $box
     *
     * @return array
     */
    public function populateTemplate(ContainerInterface $container, WidgetsEntity $box)
    {
        return [
            'box' => $box,
        ];
    }
}

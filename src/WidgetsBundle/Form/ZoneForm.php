<?php

namespace WidgetsBundle\Form;

use Kisphp\Utils\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;

class ZoneForm extends AbstractType
{
    const FIELD_TITLE = 'title';
    const FIELD_STATUS = 'status';

    /**
     * @return string
     */
    public function getName()
    {
        return 'zone';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(self::FIELD_TITLE, 'text', [
            'label' => 'Widget Zone Title',
            'constraints' => [
                new NotBlank(),
                new Required(),
            ],
        ])
        ->add(self::FIELD_STATUS, 'choice', [
            'expanded' => 'true',
            'choices' => [
                Status::ACTIVE => 'Active',
                Status::INACTIVE => 'Inactive',
            ],
            'attr' => [
                'class' => 'form-inline status-radio',
            ],
        ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}

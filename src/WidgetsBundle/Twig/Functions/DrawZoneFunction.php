<?php

namespace WidgetsBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;
use WidgetsBundle\Services\WidgetService;

class DrawZoneFunction extends AbstractTwigFunction
{
    use IsSafeHtml;

    /**
     * @var WidgetService
     */
    protected $widgetService;

    /**
     * @param WidgetService $service
     */
    public function __construct(WidgetService $service)
    {
        $this->widgetService = $service;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'drawZone';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        $widgetService = $this->widgetService;

        return function ($idZone) use ($widgetService) {
            return $widgetService->drawZone($idZone);
        };
    }
}

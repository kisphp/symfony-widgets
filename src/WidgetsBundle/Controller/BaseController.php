<?php

namespace WidgetsBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Kisphp\Entity\FileInterface;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method Registry getDoctrine()
 */
class BaseController extends Controller implements ContainerAwareInterface
{
    /**
     * @var EntityManagerInterface|ObjectManager
     */
    protected $em;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $this->getDoctrine()->getManager();
    }

    /**
     * @param string $text
     *
     * @return string
     */
    protected function trans($text)
    {
        return $this->get('translator')->trans($text);
    }

    /**
     * @param string $entityRepositoryName
     * @param int $id
     *
     * @return array
     */
    public function getEntityImages($entityRepositoryName, $id)
    {
        $id = (int) $id;
        $entity = $this->em
            ->getRepository($entityRepositoryName)
            ->find($id)
        ;

        $images = $entity->getImages();

        $imagesList = [];
        /** @var FileInterface $image */
        foreach ($images as $image) {
            $imagesList[] = [
                'title' => $image->getTitle(),
                'value' => $this->generateUrl('thumbs', [
                    'directory' => $image->getDirectory(),
                    'filename' => $image->getFilename(),
                    'width' => 400,
                    'height' => 300,
                ]),
            ];
        }

        return $imagesList;
    }

    /**
     * @param string $repository
     * @param int $objectId
     *
     * @return JsonResponse
     */
    protected function statusToggleAction($repository, $objectId)
    {
        /** @var ToggleableInterface|KisphpEntityInterface $entity */
        $entity = $this->em
            ->getRepository($repository)
            ->find($objectId)
        ;

        if (!$entity) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => $this->trans('Object not found'),
            ]);
        }

        $this->get('toggle_status_service')->toggleStatus($entity);

        $this->em->persist($entity);
        $this->em->flush();

        $response = [
            'code' => Response::HTTP_OK,
            'objectId' => $entity->getId(),
            'status' => $entity->getStatus(),
        ];

        return new JsonResponse($response);
    }
}

<?php

namespace WidgetsBundle\Controller;

use Kisphp\Utils\Status;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use WidgetsBundle\Entity\Repository\WidgetsAttachedRepository;
use WidgetsBundle\Entity\Repository\WidgetsRepository;
use WidgetsBundle\Entity\Repository\WidgetsZoneRepository;
use WidgetsBundle\Entity\WidgetsAttached;
use WidgetsBundle\Entity\WidgetsEntity;
use WidgetsBundle\Entity\WidgetsZoneEntity;
use WidgetsBundle\Form\AbstractWidgetForm;
use WidgetsBundle\Form\BoxForms\CodeForm;
use WidgetsBundle\Form\ZoneForm;

/**
 * @Template()
 */
class WidgetsController extends BaseController
{
    const FIELD_TITLE = 'title';
    const FIELD_STATUS = 'status';
    const FIELD_FORM = 'form';
    const FIELD_BOX_DATA = 'box_data';
    const FIELD_ZONE = 'zone';
    const FIELD_PRIORITY = 'priority';

    /**
     * @Route("/adm/layout/widgets/status", name="adm_layout_widgets_status")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statusAction(Request $request)
    {
        $objectId = $request->request->getInt('id');

        return $this->statusToggleAction('WidgetsBundle:WidgetsEntity', $objectId);
    }

    /**
     * @Route("/adm/layout/zone/status", name="adm_layout_zone_status")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statusZoneAction(Request $request)
    {
        $objectId = $request->request->getInt('id');

        return $this->statusToggleAction('WidgetsBundle:WidgetsZoneEntity', $objectId);
    }

    /**
     * @Route("/adm/layout/zone", name="adm_layout_zone")
     *
     * @return array
     */
    public function zoneAction()
    {
        $entities = $this->getWidgetsZoneRepository()
            ->getUndeletedWidgetZones()
        ;

        return [
            'title' => 'Layout Zones',
            'entities' => $entities,
        ];
    }

    /**
     * @Route("/adm/widgets/box/{id}", name="widgets_box_images")
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function imagesAction($id)
    {
        $widgetBox = $this->em
            ->getRepository('WidgetsBundle:WidgetsEntity')
            ->find((int) $id)
        ;

        $images = $widgetBox->getImages();

        $imagesList = [];
        /** @var WidgetsAttached $image */
        foreach ($images as $image) {
            $imagesList[] = $this->createImage($image);
        }

        return new JsonResponse($imagesList);
    }

    /**
     * @Route("/adm/layout/zone/create", name="adm_layout_zone_create", defaults={"idZone": 0})
     * @Route("/adm/layout/zone/edit/{idZone}", name="adm_layout_zone_edit")
     *
     * @param Request $request
     * @param int $idZone
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function zoneEditAction(Request $request, $idZone=0)
    {
        $zone = new WidgetsZoneEntity();
        $defaultData = [
            self::FIELD_STATUS => Status::ACTIVE,
        ];

        if ($idZone > 0) {
            $zone = $this->getWidgetsZoneRepository()->find($idZone);
            $defaultData = [
                self::FIELD_TITLE => $zone->getTitle(),
                self::FIELD_STATUS => $zone->getStatus(),
            ];
        }

        $form = $this->createForm(new ZoneForm(), $defaultData);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $zone->setTitle($data[self::FIELD_TITLE]);
            $zone->setStatus($data[self::FIELD_STATUS]);

            $this->em->persist($zone);
            $this->em->flush();

            $this->addFlash(
                'success',
                'Widget Zone was successfully saved'
            );

            return $this->redirect($this->generateUrl('adm_layout_zone_edit', [
                'idZone' => $zone->getId(),
            ]));
        }

        return [
            'title' => 'Create Widget Zone',
            'zone' => $zone,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/adm/layout/widgets/{idZone}", name="adm_layout_widgets")
     *
     * @param int $idZone
     *
     * @return array
     */
    public function widgetsAction($idZone=0)
    {
        /** @var WidgetsRepository $repo */
        $repo = $this->em->getRepository('WidgetsBundle:WidgetsEntity');
        $entities = $repo->getWidgetsForAdmin($idZone);

        $zone = $this->getWidgetsZoneRepository()
            ->find($idZone)
        ;

        return [
            'id_zone' => $idZone,
            'entities' => $entities,
            'zone' => $zone,
        ];
    }

    /**
     * @Route("/adm/layout/box-create", name="adm_layout_create")
     * @Route("/adm/layout/box/{idWidget}", name="adm_layout_box")
     *
     * @param Request $request
     * @param int $idWidget
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function boxAction(Request $request, $idWidget=0)
    {
        $box = new WidgetsEntity();
        $idZone = $request->query->getInt('id-zone');

        $defaultData = [
            self::FIELD_STATUS => Status::ACTIVE,
            self::FIELD_PRIORITY => WidgetsEntity::DEFAULT_PRIORITY,
        ];

        if ($idWidget > 0) {
            $box = $this->em
                ->getRepository('WidgetsBundle:WidgetsEntity')
                ->find($idWidget)
            ;
            $idZone = $box->getIdZone();

            $defaultData = [
                self::FIELD_TITLE => $box->getTitle(),
                self::FIELD_FORM => $box->getForm(),
                self::FIELD_STATUS => $box->getStatus(),
                self::FIELD_BOX_DATA => $box->getBoxData(),
                self::FIELD_PRIORITY => $box->getPriority(),
            ];
        }

        $formBuilder = $this->createFormBuilder($defaultData, [
                'attr' => [
                    'novalidate' => 'novalidate',
                ],
            ]);

        $formBuilder
            ->add(self::FIELD_TITLE, TextType::class, [
                'label' => 'Box Title',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add(self::FIELD_FORM, ChoiceType::class, [
                'choices' => $this->getBoxForms(),
                'empty_value' => '- Select Box Script -',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add(self::FIELD_STATUS, ChoiceType::class, [
                'expanded' => 'true',
                'choices' => [
                    Status::ACTIVE => 'Active',
                    Status::INACTIVE => 'Inactive',
                ],
                'attr' => [
                    'class' => 'form-inline status-radio',
                ],
            ])
            ->add(self::FIELD_PRIORITY, NumberType::class, [
                'label' => 'Priority',
            ])
        ;

        $widgetFormClass = $box->getForm();

        $formBuilder->add(self::FIELD_BOX_DATA, TextType::class, [
            'disabled' => true,
            'data' => 'Select the form and save to display it here',
        ]);

        $formClass = null;
        if (empty($widgetFormClass) !== true) {

            if (class_exists($widgetFormClass) === false) {
                $widgetFormClass = CodeForm::class;
            }

            /** @var AbstractWidgetForm $formClass */
            $formClass = new $widgetFormClass();
            $formBuilder->add(self::FIELD_BOX_DATA, $formClass);
        }

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        /** @var WidgetsAttachedRepository $repo */
        $repo =$this->em
            ->getRepository('WidgetsBundle:WidgetsAttached')
        ;
        $attached = $repo->findAttachedByWidget($box);

        if ($form->isValid()) {
            $data = $form->getData();

            $box->setTitle($data[self::FIELD_TITLE]);
            $box->setForm($data[self::FIELD_FORM]);
            $box->setStatus($data[self::FIELD_STATUS]);
            $box->setPriority($data[self::FIELD_PRIORITY]);
            if (array_key_exists(self::FIELD_BOX_DATA, $data)) {
                $box->setBoxData($data[self::FIELD_BOX_DATA]);
            }
            $zone = $this->getWidgetsZoneRepository()
                ->find($idZone)
            ;
            $box->setZone($zone);

            $this->em->persist($box);
            $this->em->flush();

            if ($formClass !== null) {
                $formClass->postForm($this->get('service_container'), $data);
            }

            $this->addFlash(
                'success',
                'Box was successfully saved'
            );

            return $this->redirect($this->generateUrl('adm_layout_box', [
                'idWidget' => $box->getId(),
            ]));
        }

        return [
            'title' => 'Widget Box',
            'entity' => $box,
            'idZone' => $idZone,
            'idWidget' => $idWidget,
            'upload_url' => $this->generateUrl('adm_layout_upload', ['idWidget' => (int) $box->getId()]),
            'form' => $form->createView(),
            'attached' => $attached,
        ];
    }

    /**
     * @Route("/adm/layout/widgets/{id}/remove", name="adm_layout_remove_widgets")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeWidgetAction(Request $request)
    {
        $entityId = $request->request->getInt('id');
        $listingUrl = $this->generateUrl('adm_users');

        /** @var WidgetsEntity $entity */
        $entity = $this->em
            ->getRepository('WidgetsBundle:WidgetsEntity')
            ->find($entityId)
        ;

        $response = new JsonResponse([
            'code' => Response::HTTP_NOT_FOUND,
            'message' => 'Objcet not found',
        ]);

        if ($entity) {
            $entity->setStatus(Status::DELETED);
            $this->em->persist($entity);
            $this->em->flush();

            $response = new JsonResponse([
                'code' => Response::HTTP_OK,
                'entity_id' => $entityId,
                'list_url' => $listingUrl,
            ]);
        }

        return $response;
    }

    /**
     * @return array
     */
    protected function getBoxFormsDirectories()
    {
        $directories = [
            dirname(__DIR__) . '/Form/BoxForms',
        ];

        $rootDirectory = dirname($this->get('kernel')->getRootDir());

        foreach ($this->getParameter('boxFormsNamespaces') as $boxFormDirectory) {
            $directories[] = $rootDirectory . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . $boxFormDirectory;
        }

        return $directories;
    }

    /**
     * Get widgets forms classes and display a select box in add widget box page
     *
     * @return array
     */
    protected function getBoxForms()
    {
        $finder = new Finder();
        $finder->files();
        foreach ($this->getBoxFormsDirectories() as $directory) {
            $finder->in($directory);
        }
        $finder->name('*Form.php');

        $widgetsForms = [];
        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $widgetsForms[$this->getBoxFormNamespace($file)] = $this->getBoxFormSelectName($file);
        }

        return $widgetsForms;
    }

    /**
     * @param SplFileInfo $file
     *
     * @return string
     */
    protected function getBoxFormNamespace(SplFileInfo $file)
    {
        $chunks = explode('src', $file->getPathname());
        $className = end($chunks);

        $patterns = [
            '.' . $file->getExtension(),
        ];

        $namespace = str_replace($patterns, '', $className);
        $namespace = str_replace('/', '\\', $namespace);

        return $namespace;
    }

    /**
     * @param SplFileInfo $file
     *
     * @return string
     */
    protected function getBoxFormSelectName(SplFileInfo $file)
    {
        return str_replace('.' . $file->getExtension(), '', $file->getFilename());
    }

    /**
     * @param WidgetsAttached $image
     *
     * @return array
     */
    private function createImage(WidgetsAttached $image)
    {
        return [
            'title' => $image->getTitle(),
            'value' => $this->generateUrl('thumbs', [
                'directory' => $image->getDirectory(),
                'filename' => $image->getFilename(),
                'width' => 400,
                'height' => 300,
            ]),
        ];
    }

    /**
     * @return WidgetsZoneRepository
     */
    protected function getWidgetsZoneRepository()
    {
        /** @var WidgetsZoneRepository $repo */
        $repo = $this->em->getRepository('WidgetsBundle:WidgetsZoneEntity');

        return $repo;
    }
}
